# auxiliary-channels

Code associated with LIGO auxiliary data releases.  

For context, see the [auxiliary data release on the GWOSC web site](https://gw-osc.org/auxiliary/GW170814).

## Documentation

**[Examples Directory](./GW170814/examples)** contains example code to read and plot these data

**[Channel List: chanlist.csv](./GW170814/dataprep/chanlist.csv)** contains a list of included channels.  Not all channels are available during this time period, 
and in some cases, there may be differences in which channels are available at the two LIGO sites.

**[Channel Documentation: channel_info.md](./GW170814/channel_info.md)** contains notes on how to understand the content in the various channels






# -- Set up enviornment
source /home/jonah.kanner/.bashrc
conda activate gwosc

# -- Unpack the tar ball in the current working directory
tar -xvf /loscdata/archive/AUX/aux_gw170814/AUXR_GWF.tar

# -- Clean up the new directory structure
mv loscdata/archive/NDS2/aux_gw170814/AUXR/ .
rm -r loscdata

# -- Run the check 
python check-tar.py

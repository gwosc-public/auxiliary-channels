
# Review checks for GW170814 Auxiliary Channel Release

### `routinely_check.py`: 
This check shows that the HDF5 files contain valid data.  Output is recorded in hdf-log.log

### `check-gwf-allchannels.py`: 
Gets data from public NDS2 server and checks for sample-by-sample agreement with data from LIGO internal NDS2 server.  Checks a small segment of time for every available channel.  Log file in `log_check_allchannels.log`.  

This test shows:
 * The number of channels available in the L1 and H1 data sets (See lines start with word "Checked")
 * All channels are available and readable, at least for the segment checked
 * One can download data from the public NDS2 server, and get exactly the same values from the internal server

### `check-gwf-alltime.py`   
Gets data from public NDS2 server and checks for sample-by-sample agreement with data from 
LIGO internal NDS2 server.  Checks a small number of channels at all available times.  Output can be seen in `log_check_alltime.log`.

The check shows:
 * The time-span of data available (about 3 hours)
 * All data files are valid and readable
 * One can download 3 hours of data from the public NDS2 server, and get exactly the same values from the internal server


### `run-check-tar.sh` and `check-tar.py`
Bash script to check the GWF tarball.  The script is intended to be run on losc-data machine, where the tarball is hosted.  Alternativlely, tar ball may
be downloaded from GWOSC web site to directory of choice, and unpacked, before running the python script, `check-tar.py`.  Log file is in `log_check_tar.log`

The check shows:
 * The GWF tar ball contains a full 3 hours of data
 * All files in the tar ball are valid files, which may be open and read
 * For a few channels, the script demonstrates the values contained are identical to what's available on the internal NDS2 server


### `setup.sh`: 
Run these commands to activate the conda IGWN enviornment to run the GWF tests
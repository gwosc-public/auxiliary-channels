
import gwpy
from gwpy.detector import ChannelList, Channel
from gwpy.timeseries import TimeSeries
import csv
import numpy as np
import logging

# -- Set up logging
logging.basicConfig(filename='log_check_alltime.log', filemode='w', format='%(message)s', level=logging.INFO)
logging.info('Testing the log file')

# -- Set channel list
chanfn = '../dataprep/chanlist.csv'

# -- Set start time and duration
start = 1186736512
end = 1186747264  # -- whole data set
# end = start + 512  # -- Check only first chunk to avoid asking for too much data

# -- Set IFO
ifolist = ['H1', 'L1']

# -- Read in channel list
chanlist = []
with open(chanfn, newline='') as csvfile:
    chanreader = csv.reader(csvfile)
    for row in chanreader:
        channame = row[0]
        if channame == '':
            continue
        else:
            #channame = '*:' + channame
            chanlist.append(channame)


# print(chanlist)

for ifo in ifolist:
    logging.info("Checking IFO {} ... \n\n".format(ifo))
    count = 0
    for chan in chanlist[1:5]:
    
        # -- Construct channel name
        channel = ifo + ':' + chan
        logging.info(channel)
        print(channel)

        # -- Fetch public data
        try:
            server ='losc-nds.ligo.org'  #-- GWOSC NDS2 server name
            public_data = TimeSeries.fetch(channel, start=start, end=end, host=server)
            logging.info(" ... Got {} minutes of public data".format(len(public_data)*public_data.dt / 60) )
        except:
            logging.info(" ... data not found")
            continue

        # -- Fetch internal data
        server = 'nds.ldas.cit'
        inside_data = TimeSeries.fetch(channel, start=start, end=end, host=server)

        difference = np.abs(inside_data - public_data)
        total = difference.sum()
        logging.info(" ... Difference between data streams of {:e}".format(total))
        
        if total != 0.0:
            logging.info('TEST FAILS!')
            raise
        else:
            logging.info(' ... Test passes')
            count += 1
            
    logging.info("\nChecked {0} channels for {1}\n".format(count, ifo))

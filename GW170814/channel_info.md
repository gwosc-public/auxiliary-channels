## Information about the Channels


1. What are these channels?
1. Why were these channels picked?
1. Why have the data been low-pass filtered and downsampled?
 
 
## What Are These Channels?

 The AUX channel list chosen for this list includes all of the sensors (real or virtual) which:
 
 * are part of the LIGO Physical Environment Monitor (PEM) system (e.g. seismometers, microphones, magnetometers)
 
 * non-interferometric sensors of the suspended interferometer optics (e.g. local motion sensors, "shadow" sensors)
 
 * sensors and actuators of the seismic vibration isolation platofrms (SEI)
 
 * interferometric signals which measure degrees of freedom not directly related to the gravitational-wave (differential-arm or DARM) degree of freedom


## Why were these channels picked?

This channel list contains all of the sensors (both real and virtual) which are known to:

1. couple strongly to the GW strain channel
2. measure parts of the environment which may modulate the coupling of other channels to the strain channel
3. be of auxiliary use in diagnosing other misbehavior in the interferometer or related systems
4. be of general interest in the monitoring of the environment for scientific purposes not directly related to GW detection


## Why have the data been low-pass filtered and downsampled?

The full (non-reduced) data are available as GWF files or through the LIGO NDS interface.

The HDF5 files have been reduced in size via decimation (aka low-pass filtering and downsampling) to the sample rate which is indicated for each channel in the HDF5 files. 
The purpose of the processing was to reduce the data size, both for file transfer time, as well as to reduce data processing computation load. We believe that the chosen sample
rates still represent the overwhelming majority of interesting information for each sensor. For example, the seismometer channels need not be recorded at a rate much beyond the
analog bandwidth of the instrument.
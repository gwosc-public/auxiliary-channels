"""
Script to generate AUXR data for three hour data release.
Adopted from RDS script by Greg Mendell
"""

import os
import glob
from subprocess import call
import csv

# -- New type
frtype = 'AUXR'

# -- Set top level directory where new files should live
outroot = './AUXR'

#-- Set the start and stop GPS times
dur = int(3*3600/2)  # 1.5 hours
t0 = 1186741861      # centered on GW170814
start = t0 - dur 
end = t0 + dur


# -- Read in channel list
chanlist = []
with open('chanlist.csv', newline='') as csvfile:
    chanreader = csv.reader(csvfile)
    for row in chanreader:
        channame = row[0]
        if channame is '':
            continue
        else:
            channame = '*:' + channame
            chanlist.append(channame)


chanstr = ' '.join(chanlist)
print(chanstr)

#-- Tags are used to select channels we want.  See FrCopy documentation
#tags = '*PEM-*_SEIS_*_*_DQ' 

ifoList = {'H1', 'L1'}
for ifo in ifoList:

    rootdir = '/archive/frames/O2/raw/{ifo}'.format(ifo=ifo, obs=ifo[0])
    print(rootdir)

    # -- Get list of directories
    dirList = glob.glob(rootdir+'/*')
    print(dirList)

    for directory in dirList:
        fileList = glob.glob(directory + '/*.gwf')
        head, tail = os.path.split(directory)
        dirspl = tail.split('-')
        newdir = '{zero}-{ifo}_{frtype}-{two}'.format(zero=dirspl[0], ifo=ifo, frtype=frtype, two=dirspl[2])
        outdir = os.path.join(outroot, ifo, newdir)
        print(outdir)

        for filename in fileList:
            spl = os.path.basename(filename).split('-')
            gps = int(spl[2])

            if gps>=start and gps<=end:
                if not os.path.exists(outdir):
                    os.makedirs(outdir)                
                outname = os.path.join(outdir, '{obs}-{ifo}_{frtype}-{gps}-{size}'.format(obs=ifo[0], ifo=ifo, 
                                                                                         frtype=frtype, gps=gps, size=spl[3]))
                cmd = ['FrCopy', '-i', filename, '-o', outname, '-t', chanstr]
                call(cmd)


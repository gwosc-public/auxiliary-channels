"""
Script for copying LIGO frame files into the LOSC file system
The script picks out channels set by "tags", and copies 
only those into the new frame files.  This is intended to
create data sets to be used accessed by the LOSC NDS2 server

2017 by Jonah Kanner
"""

import os
import glob
from subprocess import call

# -- New type
frtype = 'LOSCSEIS'

# -- Set top level directory where new files should live
outroot = '/loscdata/archive/NDS2/o1seis'

#-- Set the start and stop GPS times
start = 1130132928
end = start + 3600*24*3

#-- Tags are used to select channels we want.  See FrCopy documentation
tags = '*PEM-*_SEIS_*_*_DQ' 

ifoList = {'L1'}
for ifo in ifoList:

    rootdir = '/archive/frames/O1/raw/{ifo}'.format(ifo=ifo, obs=ifo[0])
    print rootdir

    # -- Get list of directories
    dirList = glob.glob(rootdir+'/*')
    print dirList

    for directory in dirList:
        fileList = glob.glob(directory + '/*.gwf')
        head, tail = os.path.split(directory)
        dirspl = tail.split('-')
        newdir = '{zero}-{ifo}_{frtype}-{two}'.format(zero=dirspl[0], ifo=ifo, frtype=frtype, two=dirspl[2])
        outdir = os.path.join(outroot, ifo, newdir)
        print outdir

        for filename in fileList:
            spl = os.path.basename(filename).split('-')
            gps = int(spl[2])

            if gps>start and gps<end:
                if not os.path.exists(outdir):
                    os.makedirs(outdir)                
                outname = os.path.join(outdir, '{obs}-{ifo}_{frtype}-{gps}-{size}'.format(obs=ifo[0], ifo=ifo, 
                                                                                         frtype=frtype, gps=gps, size=spl[3]))
                cmd = ['FrCopy', '-i', filename, '-o', outname, '-t', tags]
                call(cmd)

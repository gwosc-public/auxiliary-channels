#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
import numpy as np
import scipy.signal as sig
import scipy.stats as stats
import matplotlib.pyplot as plt
import h5py as h5
import os, sys, glob
import gpstime

import dataLib as dlib


class Error(Exception):
    pass

class ValidityError(Error):
    def __init__(self, message):
        self.message = message
        
class AliasingError(Error):
    def __init__(self, message):
        self.message = message

def find_latest_files(data_dir):
    """
    find all the h5 files containing AUXR and rank them according to the modification time
    the first one corresponds to the latest modified file
    """
    files_path = os.path.join(data_dir, '*AUXR*.h5')
    files = sorted(glob.iglob(files_path), key=os.path.getctime, reverse=True) 
    return files

def check_valid(file):
    """
    check the if all the channel marked with valid = 1 is indeed valid
    """
    fid = h5.File(file, 'r')
    flag_valid = 1
    bad_chans = []
    
    for key_top in fid.keys():
        grp_top = fid[key_top] 
        
        for key_chan in grp_top.keys():
            
            print('checking: ', key_chan)
            grp_chan = grp_top[key_chan]
            
            if ('valid' in grp_chan.attrs) & (grp_chan.attrs['valid']>0):
                # this means it should be a good channel
                try:
                    # try to read in all the required fields and make sure no KeyError
                    gps_start, duration = grp_chan.attrs['gps_start'], grp_chan.attrs['duration']
                    fs, fs0 = grp_chan.attrs['fs'], grp_chan.attrs['fs0']
                    
                    data = grp_chan['data'][()]
                    
                    # check if the data are identically zero
                    valid = dlib.checkAllZero(data)
#                     print(valid)
                    
                    if valid < 0:
                        flag_valid = -1 
                        bad_chans.append(key_chan)
                        
                except KeyError:
                    # some keys are missing!
                    flag_valid = -1
                    bad_chans.append(key_chan)
                    
    return flag_valid, bad_chans

def check_aliasing_one_fs_pair(fs0, fs, **kwargs):
    """
    check if aliasing for one (fs, fs0) pair
    where fs is the down-sampled freq and fs0 is the original freq
    """
    
    # freq range in which to check the aliasing problem
    freq_low = kwargs.pop('freq_low', 0.125) # measured in f_nyq
    freq_up = kwargs.pop('freq_up', 0.375)

    tol = kwargs.pop('tol', 2e-2) # tolerance on fractional difference
    dur = kwargs.pop('dur', 64) # simulate a white noise of dur sec
    tperseg = kwargs.pop('tperseg', 32) # time in sec per sec to compute the ASD
    
    seed = kwargs.pop('seed', 314) # random seed
    
    f_nyq = fs/2.
    freq_low *= f_nyq
    freq_up  *= f_nyq
    
    # generate a random white noise of dur sec with a sampling rate of fs0 Hz
    RS=np.random.RandomState(seed=seed)
    data0 = RS.randn(int(dur*fs0))
    
    # down-sample it to fs with the same function used for real data
    data = dlib.myDecimateFunc(data0, fs0, fs)
    
    # now compute the spectrum 
    freq0, psd0 = sig.welch(data0, fs=fs0, nperseg=int(fs0*tperseg))
    freq,  psd  = sig.welch(data,  fs=fs,  nperseg=int(fs*tperseg))
    
    # compute the fractional difference in ASD
    idx0 = (freq0>=freq_low) & (freq0<=freq_up)
    idx  = (freq >=freq_low) & (freq <=freq_up)
    
    frac_diff = np.abs(psd0[idx0] - psd[idx]) / psd0[idx0]
    frac_diff = np.sqrt(frac_diff)
    
    frac_diff = np.percentile(frac_diff, 95)
    
    if (frac_diff>tol) & (fs>1):
        flag_AA = -1 # bad anti-aliasing
    elif (fs<=1) & (frac_diff>0.15):
        # less strict condition for the very slow channels sampled at 1 Hz
        flag_AA = -1 
    else:
        flag_AA = 1
        
#     print(fs0, fs, frac_diff, tol, flag_AA)
    return flag_AA

def check_aliasing(file, **kwargs):
    """
    check if the anti-aliasing is good for all the (fs0, fs) pairs in the file
    """
    fid = h5.File(file, 'r')
    fs0_fs_pairs = np.zeros([0, 2])
    bad_pairs = np.zeros([0, 2])
    flag_AA = 1
    
    for key_top in fid.keys():
        grp_top = fid[key_top] 
        
        for key_chan in grp_top.keys():
            grp_chan = grp_top[key_chan]
            
            if ('valid' in grp_chan.attrs) & (grp_chan.attrs['valid']>0):
                fs0, fs = grp_chan.attrs['fs0'], grp_chan.attrs['fs']
                
                if fs<fs0:
                    fs0_fs = np.array([fs0, fs])
                    fs0_fs_pairs=np.vstack([fs0_fs_pairs, fs0_fs])
                    
    fs0_fs_pairs = np.unique(fs0_fs_pairs, axis=0)
#     print(fs0_fs_pairs[:5, :])
    
    n_pairs = fs0_fs_pairs.shape[0]
    for i in range(n_pairs):
        fs0_fs=fs0_fs_pairs[i, :]
        flag = check_aliasing_one_fs_pair(fs0_fs[0], fs0_fs[1], **kwargs)
        if flag<0:
            flag_AA = -1
            bad_pairs = np.vstack([bad_pairs, fs0_fs])
    return flag_AA, bad_pairs


if __name__=='__main__':
    data_dir = '/home/nlnoise/public_html/Data/aux_release/L1' # directory where the data file is stored
    
    gps_now = int(np.round(gpstime.tconvert('now')))
    
    # find the latest modified file
    files = find_latest_files(data_dir) 
    file = files[0]
#     print(file)
    
    # check if all the data are valid
    flag_valid, bad_chans = check_valid(file)
    if flag_valid<0:
        # failed the validity check
        fid = open('valid_%i.log'%gps_now, 'w')
        for i in range(len(bad_chans)):
            fid.write('%s\n'%bad_chans[i])
        fid.close()
            
        raise ValidityError('Validity check failed!!!')
        
    # check if the AA is good
    flag_AA, bad_pairs = check_aliasing(file)
    if flag_AA<0:
        # failed the AA test
        fid = open('AA_%i.log'%gps_now, 'w')
        for i in range(len(bad_pairs)):
            fid.write('%.0f\t%.0f\n'%(bad_pairs[i, 0], bad_pairs[i, 1]))
        fid.close()
        
        raise AliasingError('Anti-aliasing check failed!!!')
            
    
    
                
                


    
    
    

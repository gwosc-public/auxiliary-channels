## Auxiliary Data Preparation Scripts



## Creating data sets

    dataLib.py: Main code to read channels from google sheet and then fetch the data using NDS. 
                The decimation & zero checking functions are also provided here.    
                
                To run the code, one can do:
                    python dataLib.py --gps-start GPS_START --duration DURATION --ifo IFO
                This will grab data for IFO (ie H1 or L1) and save them into multiple h5 files.
                Each h5 file starts at an integer multiple of 64 ses and has a duration of 64 sec. 
                The routine will make sure to all the h5 files together covers the range 
                [GPS_START, GPS_START+DURATION].
                
                By default, each data file has a name '%s-%s_AUXR-%i-%i.h5'%(ifo[0], ifo, gps_start, duration)
                    (e.g. L-L1_AUXR-1186736384-64.h5)
                Currently all the h5 files are stored in the CIT cluster at 
                    /home/nlnoise/public_html/Data/aux_release/
                
                If one instead wants to bypass this 64 sec requirement 
                and start exactly at GPS_START for a duration exactly of DURATION, 
                then one can do:
                    python dataLib.py --gps-start GPS_START --duration DURATION --ifo IFO --mode -1
                    
                The list of channels to grab will be loaded from the google sheet: 
                http://bit.ly/auxgwosc
                together with the desired sampling rates. 
                Each channel will first be downsampled to the rates specified in the sheet 
                before saved to the h5 files. 
                    
                Each h5 files is stored with two layers:
                    The top layer corresponds to the three-letter abbreviation of an aLIGO subsystem
                    (e.g. ASC, LSC, PEM, ...)
                    The second layer corresponds to a channel. 
                    The key is the channel name (excluding the `H1:' part).
                    
                Each channel group contains an attribute, 'valid'.
                If valid=0, the channel does not exists or it outputs identically 0 during the period. 
                If valid=1, we further store 4 attributes for each channel: gps_start, duration, fs0, fs
                    where fs0 is the original sampling rate and fs is the rate stored in the h5 file,
                and a dataset 'data' for the down-sampled time series. 
                
    routinely_check.py: Codes for performing some routine checks of the data validity in the hdf5 files. 
                        Should be placed in the same location as the data.
                      

    make_AUXR.py: Create the GWF frame files.  Adopted from RDS data set scripts.

    make_nds_data.py: Script for copying LIGO frame files into the LOSC file system
                      The script picks out channels set by "tags", and copies
                      only those into the new frame files.  This is intended to
                      create data sets to be used accessed by the LOSC NDS2 server

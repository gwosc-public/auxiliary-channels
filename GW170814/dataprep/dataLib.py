#!/usr/bin/env python

import numpy as np
import scipy.interpolate as interp
import scipy.signal as sig
import h5py as h5
import datetime
import argparse
import os, sys

import pickle
import os.path


#########################################################
### default values that seldom changes
#########################################################

SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
SPREADSHEET_ID = '1eePtFtobKiKsTf26UZpt50KkfS8KNXRwTPnx9YxW9SM'
RANGE_NAME = "Longer list w/ ctrl sigs!A:E"

#########################################################
### main functions
#########################################################

def read_google_sheet():
    """
    reading google spreadsheet for the latest channel lists
    """
    # only locally import google related packages;
    # so that other functions can be called in an environment w/o google
    from googleapiclient.discovery import build
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request
    
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('sheets', 'v4', credentials=creds)
    
    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    values = result.get('values', [])
    
    # return lists of channels & desired fs'
    nChan = len(values)-1
    
    chan= []
    note= []
    cal = []
    unit = []
    fs  = np.zeros(nChan, dtype=np.int)
    
    cnt = 0
    for i in range(nChan):
        if len(values[i+1])==5:
            chan.append(values[i+1][0])
            fs[cnt] = values[i+1][1]
            note.append(values[i+1][2])
            cal.append(values[i+1][3])
            unit.append(values[i+1][4])
            cnt += 1
        elif len(values[i+1])==4:
            chan.append(values[i+1][0])
            fs[cnt] = values[i+1][1]
            note.append(values[i+1][2])
            cal.append(values[i+1][3])
            unit.append('')
            cnt += 1
        elif len(values[i+1]) == 3:
            chan.append(values[i+1][0])
            fs[cnt] = values[i+1][1]
            note.append(values[i+1][2])
            cal.append('')
            unit.append('')
            cnt += 1
        elif len(values[i+1]) == 2:
            chan.append(values[i+1][0])
            fs[cnt] = values[i+1][1]
            note.append('')
            cal.append('')
            unit.append('')
            cnt += 1
        else:
            continue

    fs=fs[:cnt]
    return chan, fs, note, cal, unit

def fetch_data_nds(gps_start, duration, chan_list, fs_list, note_list, cal_list, unit_list,\
                   ifo='L1', data_dir='data/', data_file='', \
                   **kwargs):
    # only import nds2 locally 
    # so that other functions can be called in an environment w/o nds2 client
    import nds2
    
    server = kwargs.pop('server', 'nds.ligo.caltech.edu') # 40m: 131.215.115.200
    port = kwargs.pop('port', 31200)
    allow_data_on_tape = kwargs.pop('allow_data_on_tape', '1')
    n_chan_per_fetch = kwargs.pop('n_chan_per_fetch', 32) # fetch this many channels per connection
                      
    n_chan = len(fs_list)
    n_fetch = np.int(np.ceil(np.float(n_chan)/np.float(n_chan_per_fetch)))
    
    # chan_list does not contain IFO: part
    chan_list_nds=[]
    for i in range(len(chan_list)):
        chan_list_nds.append('%s:%s'%(ifo, chan_list[i]))
    
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    if not data_file:
        data_file='%s-%s_AUXR-%i-%i.h5'%(ifo[0], ifo, gps_start, duration)
        
    # use 'a' so that if we want to include new channels, we can just add them to the existing h5 file
    fid=h5.File(data_dir + data_file, 'a')
    fid.attrs['gps_start']=gps_start
    fid.attrs['duration']=duration
    fid.attrs['ifo']=ifo
    
    for i in range(n_fetch):
        # reset connection to avoid timing-out
#         print('resetting connection!', i)
        conn = nds2.connection(server, port)
        conn.set_parameter('ALLOW_DATA_ON_TAPE', allow_data_on_tape)  
        
        idx0 = np.int(i*n_chan_per_fetch)
        idx1 = np.int((i+1)*n_chan_per_fetch)
        chan_list_loop=chan_list_nds[idx0:idx1]
        fs_list_loop=fs_list[idx0:idx1]
        note_list_loop=note_list[idx0:idx1]
        cal_list_loop=cal_list[idx0:idx1]
        unit_list_loop=unit_list[idx0:idx1]
        
        n_loop = len(chan_list_loop)
        try:
            # first try to grab all data in once
            data_buffer = conn.fetch(gps_start, gps_start+duration, chan_list_loop)
            
            for j in range(n_loop):
                key = chan_list_loop[j][3:] # drop the IFO: part
                key0 = key[:3]
                
                # first group channels together according to its first three letters
                # e.g., ASC, LSC, PEM, etc.
                if key0 not in fid.keys():
                    grp0=fid.create_group(key0)
                else:
                    grp0=fid[key0]
                
                # then save each channel as a group under grp0
                # if the channel has been fetched before, delete the old data 
                # and replace it with the new ones
                if key in grp0.keys():
                    del grp0[key]
                grp=grp0.create_group(key)
                
                data=data_buffer[j].data
                fs0 = data_buffer[j].sample_rate
                fs = fs_list_loop[j]
                
                valid = checkAllZero(data)
#                 # debug
#                 print('fetched!', key, fs0, fs, valid)
                
                if valid>0:
                    if fs<fs0:
                        data=myDecimateFunc(data, fs0, fs)
                    else:
                        fs=int(fs0) 
                    grp.create_dataset('data', shape=data.shape, dtype=data.dtype, data=data, \
                                       compression="gzip", compression_opts=9)
                    grp.attrs['fs']=fs
                    grp.attrs['fs0']=fs0
                    grp.attrs['note']=note_list_loop[j]
                    grp.attrs['cal']=cal_list_loop[j]
                    grp.attrs['unit']=unit_list_loop[j]
                    grp.attrs['valid']=1
                    grp.attrs['gps_start']=gps_start
                    grp.attrs['duration']=duration
                else:
                    grp.attrs['valid']=0
            
        except RuntimeError:
            # at least one channel is problematic
            # fetch each data individually
            
            print('invalid channel found when bulk fetching!')
            
            # first try resetting connection
            conn = nds2.connection(server, port)
            conn.set_parameter('ALLOW_DATA_ON_TAPE', allow_data_on_tape) 
            
            for j in range(n_loop):    
                # get each channel individually
                chan_sing = [chan_list_loop[j]]
                fs = fs_list_loop[j]
                key = chan_sing[0][3:]
                key0= key[:3]
                
                if key0 not in fid.keys():
                    grp0=fid.create_group(key0)
                else:
                    grp0=fid[key0]
                if key in grp0.keys():
                    del grp0[key]
                grp=grp0.create_group(key)
                
                try:
                    data_buffer = conn.fetch(gps_start, gps_start+duration, chan_sing)
                    data=data_buffer[0].data
                    fs0=data_buffer[0].sample_rate
                    valid=checkAllZero(data)
#                     # debug
#                     print('fetched!', key, fs0, fs, valid)
                    
                    if valid>0:
                        if fs<fs0:
                            data=myDecimateFunc(data, fs0, fs)
                        else:
                            fs=int(fs0)
                        grp.create_dataset('data', shape=data.shape, dtype=data.dtype, data=data, \
                                          compression="gzip", compression_opts=9)
                        grp.attrs['fs']=fs
                        grp.attrs['fs0']=fs0
                        grp.attrs['note']=note_list_loop[j]
                        grp.attrs['cal']=cal_list_loop[j]
                        grp.attrs['unit']=unit_list_loop[j]
                        grp.attrs['valid']=1
                        grp.attrs['gps_start']=gps_start
                        grp.attrs['duration']=duration
                    else:
                        grp.attrs['valid']=0

                except RuntimeError:
                    print('WARNING!!!', key, 'does not exist!')
                    grp.attrs['valid']=0                    
    fid.close()
            
def myDecimateFunc(xx, fs0, fs):
    down_factor = int(fs0/fs)
    fir_aa = sig.firwin(20*down_factor+2, 0.8/down_factor, \
        window='blackmanharris')
    yy=sig.decimate(xx, down_factor, \
        ftype=sig.dlti(fir_aa[1:-1], 1), zero_phase=True)
    return yy

def checkAllZero(xx, cut=1.e-25):
    if np.max(np.abs(xx))<=cut:
        valid=0
    else:
        valid=1
    return valid

if __name__=='__main__':
    # parse some args here
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--mode', type=int, default=1, 
                        help='>0 for real data fetching; \
                              <=0 for debugging')
    
    # nds
    parser.add_argument('--gps-start', type=int, default=1186736418, 
                        help='GPS starting time of the dataset; default to Aug 14, 2017, 09:00:00 UTC')
    parser.add_argument('--duration', type=int, default=1, 
                        help='Total duration of the datasets in sec')
    parser.add_argument('--ifo', type=str, default='L1', 
                        help='Which IFO?')
    parser.add_argument('--server', type=str, default='nds.ligo.caltech.edu', 
                        help='NDS server')
    parser.add_argument('--port', type=int, default=31200,
                        help='NDS port')
    parser.add_argument('--allow-data-on-tape', type=str, default='1', 
                        help='Allow data on tape?')    
    
    # data output
    parser.add_argument('--data-dir', type=str, default='data/', 
                        help='Directory to store the data')
    parser.add_argument('--data-file', type=str, default='', 
                        help='File name (should include .h5)')
    
    kwargs = parser.parse_args()
    # convert it to a dict
    kwargs=vars(kwargs)
    
    # read data from google sheet
    chan_list, fs_list, note_list, cal_list, unit_list = read_google_sheet()
    
#     for i in range(len(chan_list)):
#         print(chan_list[i], fs_list[i], note_list[i], cal_list[i])
    
#     # adding GW chan
#     chan_list = ['GDS-CALIB_STRAIN'] + chan_list
#     fs_list=np.hstack([512, fs_list])
    
    mode = kwargs['mode']
    if mode>0:
        # do serieous fetching; 
        # making sure that the starting time is an integer number of 64 s
        # and the duration per file is 64 s
        
        # fetch data from NDS
        gps_start = kwargs.pop('gps_start')
        duration = kwargs.pop('duration')
        ifo = kwargs.pop('ifo')
        data_dir = kwargs.pop('data_dir')
        data_file = kwargs.pop('data_file')
        
        gps_start_64 = int(np.floor(gps_start/64.)*64)
        gps_end_64 = int(np.ceil((gps_start+duration)/64.)*64)
        gps_start_list = np.arange(gps_start_64, gps_end_64, 64)
        nFiles = len(gps_start_list)
        
        print(gps_start_64, gps_end_64, nFiles)
        for i in range(nFiles):
            fetch_data_nds(int(gps_start_list[i]), 64, chan_list, fs_list, note_list, cal_list, unit_list,\
                   ifo=ifo, data_dir=data_dir, data_file=data_file, \
                   **kwargs)
        
    else:
        # debugging 
        # do not force multiples of 64 sec per file
        
        # fetch data from NDS
        gps_start = kwargs.pop('gps_start')
        duration = kwargs.pop('duration')
        ifo = kwargs.pop('ifo')
        data_dir = kwargs.pop('data_dir')
        data_file = kwargs.pop('data_file')
        
        fetch_data_nds(gps_start, duration, chan_list, fs_list, note_list, cal_list, unit_list,\
                   ifo=ifo, data_dir=data_dir, data_file=data_file, \
                   **kwargs)

## GW170814 auxiliary data release

Code associated with LIGO auxiliary data releases.  

For context, see the [GWOSC Auxiliary Data Release page](https://gw-osc.org/auxiliary/GW170814)

## Documentation

**[Examples Directory](./examples)** contains example code to read and plot these data

**[Channel List: chanlist.csv](./dataprep/chanlist.csv)** contains a list of included channels.  Not all channels are available during this time period, 
and in some cases, there may be differences in which channels are available at the two LIGO sites.

**[Channel Documentation: channel_info.md](./channel_info.md)** contains notes on how to understand the content in the various channels

`dataprep` contains code used to prepare the data set

`review` cotains some scripts used to check the data set before release





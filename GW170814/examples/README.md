## Auxiliary Data Example Scripts


## Reading Data

To read HDF5 files, see [ligo_aux_channel_reader.ipynb](./ligo_aux_channel_reader.ipynb). 

To fetch data from NDS2 server, see [nds2_gwpy.ipynb.ipynb](./nds2_gwpy.ipynb). 

To read GWF files, see [Gravitational Wave Software](https://www.gw-openscience.org/software/) (e.g. GWpy and PyCBC can both be used to read GWF files) 


